1. Scrape all pdfs:

```
scrapy crawl lists
```

2. Extract tables:

```
cd japancatalog_dell_com
python extract.py
```
3. Translate csv:

```
python translate.py
```
