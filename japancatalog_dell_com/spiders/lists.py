# -*- coding: utf-8 -*-
import scrapy


class ListsSpider(scrapy.Spider):
    name = 'lists'
    allowed_domains = ['dell.com']
    start_urls = ['https://japancatalog.dell.com/c/pesvr-config-guide/']


    def parse(self, response):
        urls_xpath = '//h3[contains("価格表", text())]/following::table[contains("guide", @class)][1]//td/a/@href'
        urls = [response.urljoin(url) for url in response.xpath(urls_xpath).extract()]

        for url in urls:
            yield {'url': url, 'file_urls': [url]}



