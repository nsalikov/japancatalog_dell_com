import os
import re
import csv
import time
import glob
import click
from googletrans import Translator

##########################################################
# Configuration
##########################################################

FILES_PATTERN = 'jp_csv/*.csv'
OUTPUT_DIRECTORY = 'en_csv'
USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36'
PROXIES = {
    # 'http': 'foo.bar:3128',
    # 'http://host.name': 'foo.bar:4012'
}

#######################################################
# Commands
#######################################################

@click.command()
@click.option('-o', '--output_dir', 'output_dir', default=OUTPUT_DIRECTORY, show_default=True)
@click.argument('files', default=FILES_PATTERN)
def cli(output_dir, files):
    files = glob.glob(files)
    tm = {} # translation memory

    for f in files:
        print('Reading {}'.format(f))

        input_filename = os.path.split(f)[-1]

        rows = get_rows(f)
        for k,v in get_tm(rows):
            if k not in tm:
                tm[k] = v

    print('Translating')
    untranslated_segments = [k for k,v in tm.items() if not v]
    translator = Translator(user_agent=USER_AGENT, proxies=PROXIES)
    results  = ungroup(translate(translator, group(untranslated_segments), delay=3))
    update_tm(tm, results)

    for f in files:
        input_filename = os.path.split(f)[-1]
        output_filename = input_filename.replace('.jp.csv', '.en.csv')
        output_path = os.path.join(output_dir, output_filename)

        print('Saving {}'.format(output_path))

        rows = get_rows(f)
        rows = apply_tm(tm, rows)
        save_rows(output_path, rows)

##########################################################
# Functions
##########################################################

def group(texts, max_size=3900):
    gr = ''
    gr_size = 0

    for text in texts:
        text_size = len(text)

        if gr_size + text_size > max_size:
            yield gr.strip()
            gr = text
            gr_size = text_size
        else:
            gr = gr + '\n' + text
            gr_size = gr_size + text_size

    if gr:
        yield gr.strip()


def translate(translator, texts, delay=3):
    for text in texts:
        time.sleep(delay)
        yield translator.translate(text, src='ja', dest='en')


def ungroup(translates):
    for t in translates:
        keys = t.origin.split('\n')
        values = t.text.split('\n')

        if len(keys) != len(values):
            print('Origin/translated texts are inconsistent')

        yield from zip(keys, values)


def get_rows(filename):
    with open(filename, encoding='utf-8') as fin:
        reader = csv.reader(fin)

        for row in reader:
            yield remove_newline(row)

def get_tm(rows):
    for row in rows:
        yield (row[1], None)
        yield (row[4], None)


def update_tm(tm, results):
    for k,v in results:
        if k in tm:
            tm[k] = v
        else:
            print('   Unknown key: {}'.format(k))


def apply_tm(tm, rows):
    for row in rows:
        row[1] = tm.get(row[1], row[1])
        row[4] = tm.get(row[4], row[4])

        yield row


def save_rows(filename, rows):
    with open(filename, 'w', encoding='utf-8', newline='') as fout:
        writer = csv.writer(fout)

        for row in rows:
            writer.writerow(row)


def remove_newline(lst):
    return [re.sub('[\r\n]', '', e) for e in lst]

#######################################################
# Main
#######################################################

if __name__ == '__main__':
    cli()