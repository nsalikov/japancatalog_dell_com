import os
import re
import csv
import glob
import click
import pdfplumber

# https://github.com/jsvine/pdfplumber#extracting-tables
# https://github.com/jsvine/pdfplumber/blob/master/examples/notebooks/extract-table-ca-warn-report.ipynb

##########################################################
# Configuration
##########################################################

FILES_PATTERN = 'pdf/*.pdf'
OUTPUT_DIRECTORY = 'jp_csv'

#######################################################
# Commands
#######################################################

@click.command()
@click.option('-o', '--output_dir', 'output_dir', default=OUTPUT_DIRECTORY, show_default=True)
@click.argument('files', default=FILES_PATTERN)
def cli(output_dir, files):
    files = glob.glob(files)

    for f in files:
        print('Extracting {}'.format(f))

        input_filename = os.path.split(f)[-1]
        output_filename = input_filename.replace('.pdf', '.jp.csv')
        output_path = os.path.join(output_dir, output_filename)

        raw_tables = list(get_tables(f))
        tables = list(fix_tables(raw_tables))

        print('Saving {}'.format(output_path))

        with open(output_path, 'w', encoding='utf-8', newline='') as fout:
            writer = csv.writer(fout)
            rows = [row for table in tables for row in table]

            for row in rows:
                writer.writerow(row)

##########################################################
# Functions
##########################################################

def get_tables(filename, table_settings=None):
    if not table_settings:
        table_settings = {
            "vertical_strategy": "lines",
            "horizontal_strategy": "lines",
        }

    with pdfplumber.open(filename) as pdf:
        for page in pdf.pages:
            # im = page.to_image().debug_tablefinder()
            # im.save('output.png')
            yield from page.extract_tables(table_settings=table_settings)


def fix_tables(tables):
    d = {}

    for table in tables:
        title = ' '.join(filter(None, table[0]))
        match = re.search('\s*(.*?)\s*\[\s*Module\s*Id\s*:\s*(\d+)\].*', title, re.IGNORECASE)

        if not match:
            print('   Unsupported table: {}'.format(title))
            continue

        module_id = match.group(2)
        module_name = match.group(1).strip()

        if module_id not in d:
            d[module_id] = []

        rows = table[2:]
        rows = fix_rows(rows)

        for row in rows:
            try:
                option_id, sku, item, price = list(filter(None, row))
            except:
                print('   Something wrong: {}'.format(row))
            price = price.replace('¥', '').replace(',', '').strip()
            d[module_id].append(remove_newline([module_id, module_name, option_id, sku, item, price]))

    for key,table in sorted(d.items()):
        yield table


def fix_rows(rows):

    _rows = []
    _buf = []

    for row in reversed(rows):
        if not row[0]:
            _buf.insert(0, row)
        else:
            if _buf:
                _buf.insert(0, row)
                row = buf2row(_buf)
                row[3] = fix_delimiters(row[3])
                _buf = []

            _rows.insert(0, row)

    if _buf:
        print('   _buf is not empty!')

    return _rows


def buf2row(buf):
    row = buf[0]
    for _row in buf[1:]:
        for i in range(len(_row)):
            if type(row[i]) is str and type(_row[i]) is str:
                row[i] = row[i].strip() + ' ' + _row[i].strip()

    return row


def fix_delimiters(s):
    return ', '.join(filter(None, re.split('[ ,]', s)))


def remove_newline(lst):
    return [re.sub('[\r\n]', '', e) for e in lst]

#######################################################
# Main
#######################################################

if __name__ == '__main__':
    cli()